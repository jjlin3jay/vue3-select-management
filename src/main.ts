import { createApp } from 'vue'
import App from '@/App.vue'
// 引入 ElementPlus 插件与样式
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
// 配置 element-plus 国际化
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-expect-error  忽略当前文件ts类型的检测否则有红色提示(打包会失败)
import zhCn from 'element-plus/dist/locale/zh-cn.mjs'
// svg插件需要的配置代码
import 'virtual:svg-icons-register'
// 引入自定义插件对象:注册整个项目全局组件
import gloalComponent from '@/components'
// 安装全局默认样式
import '@/styles/index.scss'
// 引入路由
import router from './router'
// 引入仓库
import pinia from './store'
// 暗黑模式需要的样式
import 'element-plus/theme-chalk/dark/css-vars.css'
// 引入路由鉴权文件
import './permisstion'
// 引入自定义指令文件
import { isHasButton } from './directive/has'

// 获取环境变量
// console.log(import.meta.env)

// 获取应用实例对象
const app = createApp(App)
// 安装 ElementPlus 插件
app.use(ElementPlus, {
  locale: zhCn // element-plus 国际化配置
})
// 安装插件
app.use(gloalComponent)
// 注册模板路由
app.use(router)
// 安装注册pinia
app.use(pinia)
// 调用自定义指令函数
isHasButton(app)

// 将应用挂载到挂载点上
app.mount('#app')
