// 用于项目logo|标题配置
export default {
  title: '蒸蚌兔运营平台', // 项目平台标题
  logo: '/pancake-logo.png', // 项目logo设置
  logoHidden: true // logo组件是否隐藏设置 true表示要
}
