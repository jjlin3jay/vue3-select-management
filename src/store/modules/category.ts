// 商品分类全局组件的小仓库
import { defineStore } from 'pinia'
import { reqC1, reqC2, reqC3 } from '@/api/product/attr'
import type { CategoryResponseData } from '@/api/product/attr/type'
import type { CategoryState } from './types/type'

const useCategoryStore = defineStore('Category', {
  state: (): CategoryState => {
    return {
      // 存储一级分类的数据
      c1Arr: [],
      // 存储一级分类的ID
      c1Id: '',
      // 存储对应一级分类下二级分类的数据
      c2Arr: [],
      // 收集二级分类的ID
      c2Id: '',
      // 存储三级分类的数据
      c3Arr: [],
      // 存储三级分类的ID
      c3Id: ''
    }
  },
  actions: {
    // 获取一级分类的方法
    async getC1() {
      // 发请求获取一级分类的数据
      const res1: CategoryResponseData = await reqC1()
      if (res1.code === 200) {
        this.c1Arr = res1.data
      }
    },
    // 获取二级分类的方法
    async getC2() {
      // 发送请求获取 一级分类下的二级分类数据(需要一级分类的ID)
      const res2: CategoryResponseData = await reqC2(this.c1Id)
      if (res2.code === 200) {
        // 存储数据
        this.c2Arr = res2.data
      }
    },
    // 获取三级分类的方法
    async getC3() {
      // 发送请求获取 二级分类下的三级分类数据(需要二级分类的ID)
      const res3: CategoryResponseData = await reqC3(this.c2Id)
      if (res3.code === 200) {
        // 存储数据
        this.c3Arr = res3.data
      }
    }
  },
  getters: {}
})

export default useCategoryStore
