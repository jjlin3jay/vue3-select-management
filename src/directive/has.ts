// 引入仓库
import pinia from '@/store'
import useUserStore from '@/store/modules/user'

// 创建仓库实例
const userStore = useUserStore(pinia)

// 定义自定义指令函数v-has -- 判断button权限
export const isHasButton = (app: any) => {
  // 获取对应的用户仓库
  // 全局自定义指令: 实现按钮的权限
  app.directive('has', {
    // mounted代表使用这个全局自定义指令的DOM|组件挂载完毕的时候会执行一次
    // el 即使用这个自定义指令的DOm元素
    // options 表示配置项，可以拿到自定义指令右侧的值
    mounted(el: any, options: any) {
      // 自定义指令右侧的数值:如果在用户信息buttons数组当中没有,就从DOM书上干掉
      if (!userStore.buttons.includes(options.value)) {
        // 原生写法：利用父节点删除DOM
        el.parentNode.removeChild(el)
      }
    }
  })
}
