// 路由鉴权：鉴权，项目中路由能不能被访问的权限的设置(某一个路由什么条件下可以访问，什么条件下不能访问)
import router from '@/router/index'
import nprogress from 'nprogress'
// 引入进度条样式,但不要加载小圆球
import 'nprogress/nprogress.css'
nprogress.configure({ showSpinner: false })
// 获取用户相关的小仓库内部token数据,去判断用户是否登陆成功
import useUserStore from './store/modules/user'
// 引入大仓库
import pinia from './store'
const userStore = useUserStore(pinia)
import setting from '@/setting'

// 全局守卫：项目中任意路由自由切换都会被触发的钩子
// 全局前置守卫
router.beforeEach(async (to: any, _from: any, next: any) => {
  document.title = `${setting.title}-${to.meta.title}`
  // to: any, from: any, next: any
  // 访问某一个路由之前的守卫
  // to: 将要访问的那个路由
  // from: 从哪个路由来的
  // next: 跳转到那个路由之后的回调函数(路由的放行函数)
  nprogress.start()
  // 获取token去判断用户登录还是未登录
  const token = userStore.token
  // 获取用户名字
  const username = userStore.username
  // 用户登录判断
  if (token) {
    // 登录后，有token,就不能再访问登录页了，直接转首页
    if (to.path === '/login') {
      next({ path: '/' })
    } else {
      // 其余先放行
      // 有用户信息，则放行
      if (username) {
        next()
      } else {
        // 若没有用户信息，在守卫这里发请求获取用户信息再放行
        try {
          // 获取用户信息后，再放行
          await userStore.userInfo()
          // 放行
          // 万一:刷新的时候是异步路由,有可能获取到用户信息、异步路由还没有加载完毕,出现空白的效果
          // 即有路径，没路由
          // 得确保获取完用户信息，加载完再放行 { ...to }
          next({ ...to, replace: true })
        } catch (error) {
          // token过期，获取不到用户信息了
          // 用户手动修改本地存储token
          // 退出登录->用户相关数据清空
          // 保证退出成功之后再跳转
          await userStore.userLogout()
          next({ path: '/login', query: { redirect: to.path } })
        }
      }
    }
  } else {
    // 用户未登录判断
    if (to.path === '/login') {
      next()
    } else {
      // 没有登录会直接跳转到登录页，但会携带像跳转的页面的参数
      next({ path: '/login', query: { redirect: to.path } })
    }
  }
})
// 全局后置守卫
router.afterEach(() => {
  // 访问某一个路由之后的守卫
  nprogress.done()
})

// 问题一：任意路由切换实现进度条业务 --nprogress
// 问题二：路由鉴权(路由组件访问权限的设置)
// 全部路由组件:登录|404|任意路由|首页|数据大屏|权限管理(三个子路由)|商品管理(四个子路由)

// 用户未登录:可以访问login,其余六个路由不能访问(指向login)
// 用户登录成功:不可以访问login[指向首页],其余的路由可以访问
