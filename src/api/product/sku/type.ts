export interface ResponseData {
  code: number
  message: string
  ok: boolean
}

// 定义SKU对象的ts类型
// 平台属性数据类型
export interface Attr {
  id?: number
  attrId: number | string // 平台属性ID
  valueId: number | string // 属性值ID
}
// 销售i属性数据类型
export interface saleArr {
  id?: number
  saleAttrId: number | string // 销售属性属性ID
  saleAttrValueId: number | string // 销售属性值ID
}
// SKU数据
export interface SkuData {
  category3Id?: string | number //三级分类的ID
  spuId?: string | number //已有的SPU的ID
  tmId?: string | number //SPU品牌的ID
  skuName?: string //sku名字
  price?: string | number //sku价格
  weight?: string | number //sku重量
  skuDesc?: string //sku的描述
  skuAttrValueList?: Attr[] // 平台属性集合(数组)
  skuSaleAttrValueList?: saleArr[] // 销售属性集合(数组)
  skuDefaultImg?: string // sku图片地址
  isSale?: number // 控制商品的上架与下架
  id?: number // 已有的SKU的ID
}

// 获取SKU接口返回的数据 -- ts类型
export interface SkuResponseData extends ResponseData {
  data: {
    records: SkuData[]
    total: number // 一共多少个SKU
    size: number // 一页几个
    current: number //  当前第几页
    orders: []
    optimizeCountSql: boolean
    hitCount: boolean
    countId: null
    maxLimit: null
    searchCount: boolean
    pages: number // 一共多少页
  }
}

// 获取SKU商品详情接口(返回数据)的ts类型
export interface SkuInfoData extends ResponseData {
  data: SkuData
}
