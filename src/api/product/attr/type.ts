// 分类相关的数据ts类型
export interface ResponseData {
  code: number
  message: string
  ok: boolean
}

// 分类ts类型
export interface CategoryObj {
  id: number | string
  name: string
  category1Id?: number
  category2Id?: number
}

// 相应的分类接口返回数据的类型
export interface CategoryResponseData extends ResponseData {
  data: CategoryObj[]
}

// 属性与属性值的ts类型

// 属性值对象的ts类型
export interface AttrValue {
  id?: number
  valueName: string
  attrId?: number
  flag?: boolean
}
// 存储每一个属性值的数组类型
export type AttrValueList = AttrValue[]
//属性对象
export interface Attr {
  id?: number
  attrName: string
  categoryId: number | string
  categoryLevel: number
  attrValueList: AttrValueList
}
// 存储每一个属性对象的数组ts类型
export type AttrList = Attr[]
// 属性接口返回的数据ts类型
export interface AttrResponseData extends ResponseData {
  data: Attr[]
}

/*
修改一个已有属性
{
  "id": 0, // 已有的属性的id
  "attrName": "string", // 已有的属性的名字 
  "attrValueList": [
    {
      "attrId": 0, // 属性值归属哪一个属性
      "id": 0, // 已有的属性值的ID
      "valueName": "string"
    }
  ],
  "categoryId": 0, // 已有的属性归属哪个三级分类
  "categoryLevel": 3, // 代表的是几级分类
}

某个三级分类添加一个新的属性
{
  "attrName": "string", // 新增的属性的名字 
  "attrValueList": [ // 新增的属性值数组
    {
      "valueName": "string"
    }
  ],
  "categoryId": 0, // 三级分类的ID
  "categoryLevel": 3, // 代表的是几级分类
}
*/
