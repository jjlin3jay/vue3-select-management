// 服务器全部接口(都具有的)返回的数据类型
export interface ResponseData {
  code: number
  message: string
  ok: boolean
}

// SPU数据的ts类型: 需要修改
export interface SpuData {
  id?: number // SPU对象的ID[新增SPU不需要携带]
  spuName: string // SPU名字
  tmId: number | string // 品牌ID
  description: string // SPU描述
  category3Id: number | string
  spuImageList: null | SpuImg[] // 商品的图片数据
  spuSaleAttrList: null | SaleAttr[] // 销售属性数据
}

// 数组：元素都是已有的数据类型
export type Records = SpuData[]

// 定义获取已有的SPU接口返回的数据ts类型
export interface HasSpuResponseData extends ResponseData {
  data: {
    records: Records
    total: number // 已有的SPU总数
    size: number // 当前页有几个已有的SPU
    current: number // 当前第几页
    searchCount: boolean
    pages: number // 一共几页
  }
}

// 单个品牌数据的ts类型
export interface Trademark {
  id: number
  tmName: string
  logoUrl: string
}
// 品牌接口返回数据(品牌数组)的ts类型
export interface AllTradeMark extends ResponseData {
  data: Trademark[]
}

// 商品图片的ts类型
export interface SpuImg {
  id?: number
  imgName?: string
  imgUrl?: string
  createTime?: string
  updateTime?: string
  spuId?: number
  name?: string
  url?: string
}
// 已有的SPU的照片墙的数据类型(即为商品图片数组)
export interface SpuHasImg extends ResponseData {
  data: SpuImg[]
}

// 已有的销售属性 值 对象ts类型
export interface SaleAttrValue {
  id?: number
  createTime?: null
  updateTime?: null
  spuId?: number
  baseSaleAttrId: number | string
  saleAttrValueName: string
  saleAttrName?: string
  isChecked?: null
}
// 存储已有的销售属性 值 数组类型
export type SpuSaleAttrValueList = SaleAttrValue[]

// 销售属性对象ts类型
export interface SaleAttr {
  id?: number
  createTime?: null
  updateTime?: null
  spuId?: number
  baseSaleAttrId: number | string
  saleAttrName: string
  spuSaleAttrValueList: SpuSaleAttrValueList
  flag?: boolean
  saleAttrValue?: string
}
// SPU已有的销售属性接口返回数据ts类型(即销售属性数组)
export interface SaleAttrResponseData extends ResponseData {
  data: SaleAttr[]
}

// 已有的全部SPU返回属性数据的ts类型
export interface HasSaleAttr {
  id: number
  name: string
}
// 对应销售属性的数组类型
export interface HasSaleAttrResponseData extends ResponseData {
  data: HasSaleAttr[]
}

// 平台属性
export interface Attr {
  attrId: number | string // 平台属性的ID
  valueId: number | string // 属性值的ID
}

// 销售属性
export interface saleArr {
  saleAttrId: number | string // 销售属性ID
  saleAttrValueId: number | string // 属性值的ID
}

// 新增SKU数据类型
export interface SkuData {
  category3Id: string | number //三级分类的ID
  spuId: string | number //已有的SPU的ID
  tmId: string | number //SPU品牌的ID
  skuName: string //sku名字
  price: string | number //sku价格
  weight: string | number //sku重量
  skuDesc: string //sku的描述
  skuAttrValueList?: Attr[] // 平台属性的收集
  skuSaleAttrValueList?: saleArr[] // 销售属性的收集
  skuDefaultImg: string //sku图片地址
}
// 获取SKU数据接口的ts类型 即SKU数组
export interface SkuInfoData extends ResponseData {
  data: SkuData[]
}
