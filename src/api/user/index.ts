//统一管理咱们项目用户相关的接口
import request from '@/utils/request'
import type {
  loginFormData,
  loginResponseData,
  userInfoReponseData
} from './type'
//项目用户相关的请求地址
enum API {
  LOGIN_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/index/login',
  USERINFO_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/index/info',
  LOGOUT_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/index/logout'
}

//登录接口
export const reqLogin = (data: loginFormData) =>
  // 第二个参数是约束返回数据的TS类型
  request.post<any, loginResponseData>(API.LOGIN_URL, data)
//获取用户信息
export const reqUserInfo = () =>
  request.get<any, userInfoReponseData>(API.USERINFO_URL)
//退出登录
export const reqLogout = () => request.post<any, any>(API.LOGOUT_URL)
