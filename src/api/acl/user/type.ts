// 账号信息的ts类型
export interface ResponseData {
  code: number
  message: string
  ok: boolean
}
// 代表一个账号信息的ts类型
export interface User {
  id?: number
  createTime?: string // 创建时间
  updateTime?: string // 更新时间
  username?: string
  password?: string
  name?: string
  phone?: null
  roleName?: string // 职位
}
// 数组包含全部的用户信息
export type Records = User[]
// 获取全部用户信息接口返回的数据ts类型
export interface UserResponseData extends ResponseData {
  data: {
    records: Records
    total: number // 用户总数
    size: number // 一页几个
    current: number // 当前第几页
    pages: number // 总共页数
  }
}

// 一个职位的ts类型
export interface RoleData {
  id?: number // 已有职位的ID
  createTime?: string
  updateTime?: string
  roleName: string // 职位名
  remark: null
}
// 全部职位的数组列表
export type AllRole = RoleData[]
// 获取全部职位的接口返回数据的ts类型
export interface AllRoleResponseData extends ResponseData {
  data: {
    assignRoles: AllRole // 当前用户已有的职位
    allRolesList: AllRole // 所有的职位
  }
}
// 给用户分配职位接口携带参数的ts类型
export interface SetRoleData {
  roleIdList: number[] // 分配职位ID
  userId: number // 用户ID
}
