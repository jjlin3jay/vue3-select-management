export interface ResponseData {
  code: number
  message: string
  ok: boolean
}

// 每个职位的ts类型
export interface RoleData {
  id?: number
  createTime?: string
  updateTime?: string
  roleName: string
  remark?: null
}
// 全部职位数组的ts类型
export type Records = RoleData[]
// 获取全部职位的返回数据相应的ts类型
export interface RoleResponseData extends ResponseData {
  data: {
    records: Records
    total: number
    size: number
    current: number
    orders: []
    optimizeCountSql: boolean
    hitCount: boolean
    countId: null
    maxLimit: null
    searchCount: boolean
    pages: number
  }
}

// 菜单与按钮权限数据的ts类型(每个选项)
export interface MenuData {
  id: number
  createTime: string
  updateTime: string
  pid: number
  name: string
  code: string
  toCode: string
  type: number
  status: null
  level: number // 几级权限(在第几层)
  children?: MenuList // 子菜单
  select: boolean // 勾|不勾
}
// 权限数据的数组ts类型
export type MenuList = MenuData[]
// 菜单权限与按钮权限返回数据的ts类型
export interface MenuResponseData extends ResponseData {
  data: MenuList // data中存放的就是分配权限选项的数组
}
