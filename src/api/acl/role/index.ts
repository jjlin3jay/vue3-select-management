// 角色(职位)管理模块的接口
import request from '@/utils/request'
// 引入ts类型
import type { RoleResponseData, RoleData, MenuResponseData } from './type'

// 枚举地址
enum API {
  // 获取全部职位的接口
  ALLROLE_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/role/',
  // 新增岗位的接口地址
  ADDROLE_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/role/save',
  // 修改(更新)已有的职位
  UPDATEROLE_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/role/update',
  // 获取全部的菜单与按钮的数据
  ALLPERMISSTION = 'http://114.115.179.162:8022/prod-api/admin/acl/permission/toAssign/',
  // 给相应职位分配权限
  SETPERMISTION_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/permission/doAssign/',
  // 删除已有的职位
  REMOVEROLE_URL = 'http://114.115.179.162:8022/prod-api/admin/acl/role/remove/'
}

// 获取全部的职位(角色)
export const reqAllRoleList = (page: number, limit: number, roleName: string) =>
  request.get<any, RoleResponseData>(
    API.ALLROLE_URL + `${page}/${limit}/?roleName=${roleName}`
  )
// 添加职位与修改(更新)已有职位的接口
export const reqAddOrUpdateRole = (data: RoleData) => {
  if (data.id) {
    // 修改(更新)
    return request.put<any, any>(API.UPDATEROLE_URL, data)
  } else {
    // 添加
    return request.post<any, any>(API.ADDROLE_URL, data)
  }
}
// 获取全部菜单与按钮权限的数据
export const reqAllMenuList = (roleId: number) =>
  request.get<any, MenuResponseData>(API.ALLPERMISSTION + roleId)
// 给相应的职位分配权限 (职位ID,权限ID)
export const reqSetPermisstion = (roleId: number, permissionId: number[]) =>
  request.post<any, any>(
    API.SETPERMISTION_URL + `?roleId=${roleId}&permissionId=${permissionId}`
  )
// 删除已有的职位
export const reqRemoveRole = (roleId: number) =>
  request.delete<any, any>(API.REMOVEROLE_URL + roleId)
