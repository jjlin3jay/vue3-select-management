// 数据类型的定义
export interface ResponseData {
  code: number
  message: string
  ok: boolean
}
// 菜单数据与按钮数据的ts类型
export interface Permisstion {
  id?: number // 唯一标识
  createTime: string
  updateTime: string
  pid: number
  name: string
  code: null
  toCode: null
  type: number
  status: null
  level: number // 几级菜单
  children?: PermisstionList // 子菜单
  select: boolean // 勾中
}
// 菜单对象数组的ts类型
export type PermisstionList = Permisstion[]
// 菜单接口返回数据的ts类型,data里面是对象数组
export interface PermisstionResponseData extends ResponseData {
  data: PermisstionList
}

// 添加与修改菜单携带参数的ts类型
export interface MenuParams {
  id?: number // ID
  code: string // 权限数值
  level: number // 几级菜单
  name: string // 菜单的名字
  pid: number // (点击的当前)菜单的ID,就是给哪个菜单添加|修改
}
